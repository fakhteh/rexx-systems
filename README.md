# README #

This project get json data and save it in database.
It shows the list of bookings with filters for the employee name, event name and date and reset the list.
It has a versionComparison class that defines the edge version for comparing with selected version 
to return the selected item's timezone.

For testing the project please create a database named "rexx-db",
and this table:

CREATE TABLE bookings
				  (
				  item_id       		mediumint UNSIGNED AUTO_INCREMENT primary key ,
				  employee_name      	varchar(250) ,
				  employee_mail			varchar(250) ,
				  event_id				mediumint ,
				  event_name 			varchar(250),
				  participation_fee  	DECIMAL,
				  event_date  			DATETIME ,
				  version  				varchar(15)
				  );
				  
				  
and put it in the root directory.				  
				  