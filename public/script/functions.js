var projectDir = "rexx-systems";
/**************************************/
function addJson(){
	$.post( `/${projectDir}/home/addJson`, {} ,function( data ) {
		let resData = JSON.parse(data);
		let res = resData['res']; let message = resData['message']; 
		if(res){
			filterList()
		}
		showModel('Adding json',message);
	});
}

function filterList(){
	$.ajax({
		   type: "POST",
		   url: `/${projectDir}/home/filterList`, 
		   data: $("#filter_frm").serialize(), 
		   success: function(result) {
				$('#list_view_div').html(result);
		   },                
		   error : function(req, status, error) {
				showModel('Filtering',filteringErrorMsg)   
		   }
	   });
}

function resetList(){
	$.ajax({
	   type: "POST",
	   url: `/${projectDir}/home/resetList`, 
	   success: function(result) {
		 $('#list_view_div').html(result);
	   },                
	   error : function(req, status, error) {
			showModel('Filtering',resetErrorMsg)   
	   }
   });
}

function versionComparison(itemId){
	$.post( `/${projectDir}/home/versionComparison`, { itemId : itemId } ,function( data ) {
	  showModel('This item Timezone',data)
	});
}

function showModel(header,body){
	$('#myModal_head').html(header);
	$('#myModal_body').html(body);
	$('#myModal').modal('show');
}