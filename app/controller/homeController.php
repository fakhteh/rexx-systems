<?php
require(CONFIG.'Constants.php');
require(UTILITIES.'versionComparison.php');
use utilities\versionComparison;

class homeController extends Controller {
	protected $inventory = [];
	
	public function index(){
		$this->model('booking');
		$booking_list = $this->model->selectAll();
		$this->view('home/index',[ "booking_list" => $booking_list ]);
		$this->view->set_page_title("Home");
		$this->view->render();
	}
	
	public function readJson(){
		$this->model('booking_json');
		return $this->model->readJsonFile();
	}
	
	public function addJson(){
		$this->inventory =$this->readJson();
		$this->model('booking');
		$row_count=$this->model->rowCount();
		if($row_count[0]['Count']>0){
			$add_result = false;
			$message = Constants::$already_saved;
		}else{
			$add_result = true;
			foreach($this->inventory as $key => $value){
				$item_id 			= $value['participation_id'];
				$employee_name 		= $value['employee_name'];
				$employee_mail 		= $value['employee_mail'];
				$event_id 			= $value['event_id'];
				$event_name 		= $value['event_name'];
				$participation_fee 	= $value['participation_fee'];
				$event_date 		= $value['event_date'];
				$version 			= $value['version'];
				$this->model->setItem($item_id,$employee_name,$employee_mail,$event_id,$event_name,
								$participation_fee,$event_date,$version);
				$res_add_item = $this->model->addItem();
				if(!$res_add_item){$add_result = false;}
			}
			$message = ($add_result)? Constants::$save_success_msg:Constants::$save_error_msg;
		}
		$data = array("res" => $add_result, "message" => $message);
		echo json_encode($data);
	}
	
	public function filterList(){
		$filter_txt = trim(htmlspecialchars($_POST['filterTxt']));
		$filter_by  = htmlspecialchars($_POST['filterBy']);
		$this->model('booking');
		$booking_list = $this->model->selectFilter($filter_by,$filter_txt);
		
		$this->view('home/list',[ "booking_list" => $booking_list ]);
		
		$this->view->renderAjax();
	}
	
	public function resetList(){
		$this->model('booking');
		$booking_list = $this->model->selectAll();
		$this->view('home/list',[ "booking_list" => $booking_list ]);
		
		$this->view->renderAjax();
	}
	
	public function versionComparison(){
		$item_id = htmlspecialchars($_POST['itemId']);
		$this->model('booking');
		$this->model->setItemId($item_id);
		$this->model->searchItem();
		$booking_details = $this->model->getItem();
		
		$versionComparison = new versionComparison();
		$versionComparison->setCurrentVersion($booking_details['version']);
		$current_timezone = $versionComparison->currentTimezone();
		echo $current_timezone;
	}
}
?>