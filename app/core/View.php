<?php
class View {
	private $view_file;
	private $view_data;
	private $page_title = "rexx systems";
	
	public function __CONSTRUCT($view_file,$view_data){
		$this->view_file  = $view_file;
		$this->view_data  = $view_data;
	}
	
	public function render(){
		include(VIEW . 'index' . '.phtml');
	}
	
	public function renderAjax(){
		include(VIEW . $this->view_file . '.phtml');
	}
	
	public function set_page_title($page_title){
		$this->page_title .= ($page_title != "")? " | ".$page_title : "";
	}
}
?>