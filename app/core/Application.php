<?php
class Application {
	protected $controller = '';
	protected $action = '';
	protected $prams = [];
	
	public function __CONSTRUCT(){
		
		//echo $_SERVER['REQUEST_URI'];
		$this->prepareURL();
		//echo $this->controller."<br />".$this->action;
		if(file_exists( CONTROLLER. $this->controller . '.php')){
			$this->controller = new $this->controller();
			if(method_exists($this->controller,$this->action)){
				call_user_func_array([$this->controller,$this->action],$this->prams);
			}
		}
	}
	
	private function prepareURL(){
		$request = str_replace(PROJECTDIR,"",$_SERVER['REQUEST_URI']);
		//$request = ltrim($request,"\/mvc_exercise_5\/");echo "$request<br />";
		$request = rtrim($request,'/');
		if(!empty($request)){
			$url = explode('/',trim($request));
			$this->controller = isset( $url[0] ) ? $url[0].'Controller' : 'homeController';
			$this->action = isset( $url[1] ) ? $url[1] : 'index';
			unset($url[0],$url[1]);
			$this->prams = !empty($url) ? array_values($url) : [];
		}else{
			$this->controller = 'homeController';
			$this->action = 'index';
		}
	}
}
?>