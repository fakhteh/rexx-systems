<?php
namespace utilities;

/*
* defien the edge versoin for comparing.
* get the current version and return current timezone.
*/
class versionComparison{
	protected $current_version = '';
	protected $current_timezone = '';
	protected $compared_version = '';
	
	/*
	* defien the edge versoin.
	*/
	public function __CONSTRUCT(){
		$this->compared_version = '1.0.17+60';
	}
	
	/*
	@param $current_version the current version, for example:'1.0.17+59'
	*/
	public function setCurrentVersion($current_version){
		$this->current_version = $current_version;
	}
	
	/*
	@return string 
	*/
	public function currentTimezone(){
		if($this->current_version < $this->compared_version){
			$this->current_timezone = "Europe/Berlin";
		}else{
			$this->current_timezone = "UTC";
		}
		return $this->current_timezone;
	}
}
?>