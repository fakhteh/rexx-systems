<?php
class booking_json {
	protected static $data_file;
	
	public function __CONSTRUCT(){
		self::$data_file = DATA . 'booking.json';
	}
	
	public function readJsonFile(){
		if(file_exists(self::$data_file)){
			$json = file_get_contents(self::$data_file);
			$bookingArray = json_decode($json, true);
			return($bookingArray);
		}
	}
}
?>