<?php
require(CONFIG.'Error_Log.php');
use log_errors\Error_Log;
//*************************************************************
//*************     	booking Class      ********************
//*************************************************************

class booking
{
private  $item_id = 0;
private  $employee_name = "";	
private  $employee_mail = "";
private  $event_id = 0;	
private  $event_name = '';
private  $participation_fee = '';
private  $event_date = '';
private  $version = '';	

private  $db;
private  $Table_Name = "bookings";
private  $Query_Result;
//------------------------------------------------------------------------------------------------------
/** 
* Constructor for booking class.
* @return void
*/
public function __construct($db)
{
	$this->db=$db;
}
///-------------------------------------------------------------------------------------------------------------
//-----------Start Function For Table Booking
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called by another php script. Create booking table.
* @access private
* @return boolean
*/
private function Create_T_Booking()
{
	$sql="CREATE TABLE ".$this->Table_Name."
				  (
				  item_id       		mediumint UNSIGNED AUTO_INCREMENT primary key ,
				  employee_name      	varchar(250) ,
				  employee_mail			varchar(250) ,
				  event_id				mediumint ,
				  event_name 			varchar(250),
				  participation_fee  	DECIMAL,
				  event_date  			DATETIME ,
				  version  				varchar(15)
				  );";
	$this->db->query($sql);
	return $this->db->execute();
}
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called by another php script. Delete Booking table.
* @access private
* @return boolean
*/
private function Delete_T_Booking()
{
	$sql="DROP TABLE $this->Table_Name;";
	$this->db->query($sql);
	return $this->db->execute();
}
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called by another php script. Clear booking table.
* @access private
* @return boolean
*/
private function Clear_T_Booking()
{
	$sql="Delete From $this->Table_Name;";
	$this->db->query($sql);
	return $this->db->execute();
}
/** 
* Method to be called by another php script. Check the DP_Recipe table if it exists.
* @access public
* @return boolean
*/
private function Check_T_Booking_Exist()
{
 		//'select 1 from `table_name` LIMIT 1'
	try{
		$this->db->query('SELECT 1 FROM '.$this->Table_Name);
		$result = $this->db->resultset();
		
	}catch (\Exception $e)
  	{ 
	  return false; 
  	} 
	 return $result !== FALSE;
}
//------------------------------------------------------------------------------------------------------
//-----------End Function For Table Booking
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//-----------Start Function For Tables
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called in dbcontrol. Create Booking class.
* @access public
* @return true if operation done successfully
*/
public function Create_DB()
{
		$sw=$this->Create_T_Booking();
		return $sw;
}
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called in dbcontrol. Clear the Booking class.
* @access public
* @return true if operation done successfully
*/
public function Clear_DB()
{
		$sw=$this->Clear_T_Booking();
		return $sw;
}
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called in dbcontrol. Delete the Booking class.
* @access public
* @return true if operation done successfully
*/
public function Delete_DB()
{
		$sw=$this->Delete_T_Booking();
		return $sw;
}
//------------------------------------------------------------------------------------------------------
/** 
* Method to be called in dbcontrol. Check if the table Exists.
* @access public
* @return true if operation done successfully
*/
public function Check_DB_Exist()
{
		$sw=$this->Check_T_Booking_Exist();
		return $sw;
}
//------------------------------------------------------------------------------------------------------
//----------- End Function For Tables
//------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//----------- Booking Functions
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
public function getItemId()
{
	return $this->item_id;
}
	
public function setItemId($item_id)
{
	$this->item_id = $item_id;
}	
//------------------------------------------------------------------------------------------------------
public function setItem($item_id,$employee_name,$employee_mail,$event_id,$event_name,
						$participation_fee,$event_date,$version)
{
	$this->item_id = $item_id;
	$this->employee_name = $employee_name;
	$this->employee_mail = $employee_mail;
	$this->event_id = $event_id;
	$this->event_name = $event_name;
	$this->participation_fee = $participation_fee;
	$this->event_date = $event_date;
	$this->version = $version;
}
//------------------------------------------------------------------------------------------------------
public function getItem()
{
	$booking['item_id']  = $this->item_id;
	$booking['employee_name']  = $this->employee_name;
	$booking['employee_mail']  = $this->employee_mail;
	$booking['event_id']  = $this->event_id;
	$booking['event_name']  = $this->event_name;
	$booking['participation_fee']  = $this->participation_fee;
	$booking['event_date']  = $this->event_date;
	$booking['version']  = $this->version;
	
	return $booking;
}
//------------------------------------------------------------------------------------------------------
/** 
* Add a record to  booking table.
* @access public
* @return var true if operation done successfully else return false	
*/
public function addItem()
{
	try {
		$this->db->query('INSERT INTO '.$this->Table_Name.' ( 
				  employee_name
				 ,employee_mail
				 ,event_id
				 ,event_name
				 ,participation_fee
				 ,event_date
				 ,version
				) 
				values 
				(	         
				   :employee_name
				  ,:employee_mail
				  ,:event_id
				  ,:event_name
				  ,:participation_fee
				  ,:event_date
				  ,:version
				) '
		); 

		$this->db->bind(':employee_name',		$this->employee_name);
		$this->db->bind(':employee_mail',		$this->employee_mail);
		$this->db->bind(':event_id',			$this->event_id);
		$this->db->bind(':event_name',			$this->event_name);
		$this->db->bind(':participation_fee',	$this->participation_fee);
		$this->db->bind(':event_date',			$this->event_date);
		$this->db->bind(':version',				$this->version);
		
		$add_res = $this->db->execute();
		$this->item_id = $this->db->lastInsertId();
		return $add_res;
	}catch (\Exception $e)
	{
		$this->__Error_Handeler($e->getMessage()); 
		$this->log_error('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
		
        return false; 
	}
}
//------------------------------------------------------------------------------------------------------
/** 
* select all records.
* @access public
* @return recirds array if operation done successfully else return false	
*/	
public function selectAll(){
	try{
		$this->db->query('SELECT * FROM '.$this->Table_Name.' ORDER BY item_id DESC');
		$rows = $this->db->resultset();
		return $rows;

	}catch (\Exception $e)
  	{ 
	  $this->__Error_Handeler("2001", $e); 
	  return false; 
  	} 
}
//------------------------------------------------------------------------------------------------------
public function selectFilter($filter_by,$filter_txt){
	try{
		$sql  = 'SELECT * FROM '.$this->Table_Name. ' ';
		$sql .= 'WHERE '.$filter_by.' LIKE "%'.$filter_txt.'%" ';
		$sql .=	'ORDER BY item_id DESC';
		$this->db->query($sql);
		$rows = $this->db->resultset();
		return $rows;

	}catch (\Exception $e)
  	{ 
	  $this->__Error_Handeler("2001", $e);
		$this->log_error('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
	  return false; 
  	} 
}		
//------------------------------------------------------------------------------------------------------
public function rowCount()	{
	try{
		$sql = 'SELECT COUNT(*) AS Count FROM '.$this->Table_Name;
		$this->db->query($sql);
		$rows = $this->db->resultset();
		return $rows;

	}catch (\Exception $e)
  	{ 
	  $this->__Error_Handeler("2001", $e); 
	  return false; 
  	} 
}
//------------------------------------------------------------------------------------------------------
/*
* Method Search_Item . search records with an especific id from booking table.
* @access public
* @return var true if search operation find any record else return false	
*/
public function searchItem()
{
	try{
		$this->db->query('SELECT * FROM '.$this->Table_Name.' WHERE item_id = :item_id');
		$this->db->bind(':item_id', $this->item_id );
		$rows = $this->db->resultset();
		
		$this->item_id 				= 	$rows[0]['item_id'];
		$this->employee_name  		=	$rows[0]['employee_name'];
		$this->employee_mail  		=	$rows[0]['employee_mail'];
		$this->event_id  			=	$rows[0]['event_id'];
		$this->event_name 			= 	$rows[0]['event_name'];
		$this->participation_fee  	=	$rows[0]['participation_fee'];
		$this->event_date  			=	$rows[0]['event_date'];
		$this->version  			=	$rows[0]['version'];
		
		return true;

	}catch (\Exception $e)
  	{ 
	  	$this->__Error_Handeler($e->getMessage()); 
		 $this->log_error('Fakhteh',"MySQL Error: " . $e->getMessage(),__FILE__.' Line:'.__LINE__,__METHOD__);	
         return false;  
  	} 
}
	
//------------------------------------------------------------------------------------------------------
private function log_error($User, $Err, $Location,$method)
{
	new Error_Log(ROOT);
	Error_Log::DP_SetError($User, $Err, $Location,$method);

}	
//--------------------------------------------------------------------------------------------------------
private function  __Error_Handeler($Exception)
 {
	echo "Exception=>" .$Exception;
 }	
//-------------------------------------------------------------------------------------------------------	
}

?>
