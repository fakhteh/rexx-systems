<?php
namespace log_errors;
/*
* This class will allow you to log errors
* @author Fakhteh Razazan <fakhteh.razazan@gmail.com>
* where that there is saved errors
* the separetor between errors
* date format
*/
class Error_Log
{
   	private static $LogError_Address="";
	private static $Separator="";
	private static $DateFormat="";
	
	public function __construct($root_dir){
		$this->format_setting($root_dir);
		$this->ini_Set_Setting();
	}
	/*
	* create other errors that don't created in log_error such as mysql_error
	* @access public
	* @param User (user's name)
	* @param Err (error text)
	* @param Location (where the error occurs)
	*/
	public static function DP_SetError($User, $Err, $Location,$method)
    {
		if(file_exists(self::$LogError_Address)){ 
			$Date = date(self::$DateFormat);
			$Error =self::$Separator;
			$Error .="User: ".$User."\r\n";
			$Error .="Date: ".$Date."\r\n";
			//----------------------------------
			if($method!="" && $method!=NULL){
				$Error .= "Class: ".$method."\r\n";
			}
			//---------------------------------
			$Error .= "Caller: \r\n";
			$loc_arr=explode('%%',$Location);
			for($i=0; $i<count($loc_arr); $i++){
				$Error .=$loc_arr[$i]."\r\n";
			}
			//---------------------------------
			$Error .= "Error: \r\n";
			$err_arr=explode('%%',$Err);
			foreach($err_arr as $err_val)
				$Error .= $err_val."\r\n";
			//---------------------------------
			$Error .=self::$Separator;
			
			error_log($Error, 3, self::$LogError_Address);
		}
    }
	/*
	* @access public
	* Set up error login
	* The first two lines tell PHP to report every error
	* Line three says that those errors should be logged
	* The next line makes sure that these errors won’t include HTML
	* line five sets the path to the log file. 
	* The last line is to prevent the errors from being displayed to the user
	*/
	private function ini_Set_Setting(){
		ini_set('error_reporting', E_ALL);//The first two lines tell PHP to report every error
		error_reporting(E_ALL);
		ini_set('log_errors',TRUE);//this line says that those errors should be logged
		ini_set('html_errors',TRUE);//makes sure that these errors won’t include HTML
		ini_set('error_log', self::$LogError_Address);//it sets the path to the log file
		ini_set('display_errors',FALSE);//this line is to prevent the errors from being displayed to the user /**/
	}
	
	private function format_setting($root_dir){
		self::$LogError_Address = $root_dir."log/errors.log";
		self::$Separator = "\r\n\r\n---------------------------------------------------\r\n\r\n";
		self::$DateFormat = "Y-m-d  H:i:s";
	}
}
?>