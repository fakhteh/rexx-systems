<?php
require("./app/model/utilities/versionComparison.php");
use utilities\versionComparison;
class versionComparisonTest extends PHPUnit\Framework\TestCase
{
	private $versionComparison;
 
    public function test_get_timezone()
    {
		$this->versionComparison = new versionComparison();
		
		$this->versionComparison->setCurrentVersion('1.0.17+42');
        $this->assertEquals("Europe/Berlin", $this->versionComparison->currentTimezone());
		
		$this->versionComparison->setCurrentVersion('1.0.17+65');
        $this->assertEquals("UTC", $this->versionComparison->currentTimezone());
    }
}
?>